import axios from 'axios'

const http = axios.create({
  baseURL: 'http://63613c61.ngrok.io',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export {
  http
}
