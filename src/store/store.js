import Vue from 'vue'
import Vuex from 'vuex'
import game from './modules/game'
import cart from './modules/cart'

Vue.use(Vuex)

/* eslint no-shadow: ["error", { "allow": ["state"] }] */
const state = {
  sharedRefs: {}
}

const mutations = {
  addRef(state, refs) {
    Object.assign(state.sharedRefs, refs)
  }
}

export default new Vuex.Store({
  state,
  mutations,
  modules: {
    game,
    cart
  }
})
