import gameAPI from '../../api/game'
/* eslint no-shadow: ["error", { "allow": ["state"] }] */

const state = {
  cart: []
}

const getters = {
  itemQuantity: state => state.cart.length
}

const mutations = {
  ADD_TO_CART(state, game) {
    state.cart.push(game)
    console.log(state.cart)
  }
}

const actions = {
  UPDATE_CART_FROM_LOCAL({
    commit
  }, localCart) {
    for (let i = 0; i <= localCart.length; i++) {
      gameAPI.getInfo(localCart[i].GameID).then(r => r.data).then((data) => {
        commit('ADD_TO_CART', data)
      })
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
