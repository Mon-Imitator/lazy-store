import gameAPI from '../../api/game'


/* eslint no-shadow: ["error", { "allow": ["state"] }] */
const state = {
  searchByName: true,
  searchKey: null,
  sharedRefs: {},
  gameList: [],
  gameInfo: []
}

const mutations = {
  changeSearchType(state) {
    state.searchByName = !state.searchByName
  },
  changeSearchKey(state, key) {
    state.searchKey = key
  },
  SET_SEARCH_LIST(state, payload) {
    state.gameList = payload || []
  },
  SET_GAME_INFO(state, payload) {
    state.gameInfo = payload || []
    console.log(state.gameInfo)
  },
  CLEAR_GAME_INFO(state) {
    state.gameInfo = []
  },
  CLEAR_GAME_LIST(state) {
    state.gameList = []
    console.log(state.gameList)
  }
}

const actions = {
  LOAD_SEARCH_LIST({
    commit,
    state
  }) {
    const searchType = state.searchByName ? 'name' : 'link'
    gameAPI.search({
      searchType,
      searchKey: state.searchKey
    })
      .then(r => r.data)
      .then((data) => {
        commit('SET_SEARCH_LIST', data)
      })
  },
  LOAD_GAME_INFO({
    commit
  }, appid) {
    gameAPI.getInfo(appid)
      .then(r => r.data)
      .then((data) => {
        commit('SET_GAME_INFO', data)
      })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
