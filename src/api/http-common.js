import axios from 'axios'

export default
axios.create({
  baseURL: 'http://d8696cc8.ngrok.io/api/',
  withCredentials: false,

  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})
