import http from './http-common'

const path = '/game'

const search = ({
  searchType,
  searchKey
}) => http.get(`${path}/search?${searchType}=${searchKey}`)

const getInfo = appid => http.get(`${path}/${appid}`)

export default {
  search,
  getInfo
}
